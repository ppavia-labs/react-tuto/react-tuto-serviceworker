# REACT

## Installation
### axios
```
npm install axios
```
### moment
```
npm install moment
```

## Things to note about a service worker
- a service worker **can't** access to the DOM directly. A service worker can communicate with the pages it controls by responding to messages sent via the **postMessage** interface.
- a service worker is a programmable network proxy, allowing you to control how network requests from the page are handled.
- it's terminated when not in use, and restarted when it's next needed, so we cannot rely on global state within a service worker's `onfetch` and `onmessage` handlers. If there is information that we need to persist and reuse across restarts, service workers do have access to **IndexedDP API**.
- service workers make extensive use of promise.

## The service worker lifecycle 
- To install a service worker, we need to register it. Registering a service worker will cause the browser to start the service worker install step in background.
- During the install step, some static assets could be cached. If all the files are cached successfully, then the service worker becomes installed.

### Simplified version of a service worker lifecycle on its first installation
![service worker life cycle](/sw-lifecycle.png)